import { combineReducers } from 'redux'
import moduleReducers from '../../modules/recudersList'

const rootReducer = combineReducers({
  port: moduleReducers.port,
  dialog: moduleReducers.dialog,
  notifications: moduleReducers.notifications
})

export default rootReducer