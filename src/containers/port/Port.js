import React, { Component } from 'react'
import axios from '@axios/flinkeFolkInstance'
import { connect } from 'react-redux'

import Ship from '@components/ship/Ship'
import Dock from '@components/dock/Dock.js'
import Notification from '@components/common/Notification/Notification'

import Grid from '@material-ui/core/Grid'

import AlertDialog from '@components/dialog/AlertDialog'
import EditItemDialog from '@components/dialog/EditItemDialog'

import Loader from '@components/common/Loader/Loader'

import {
  apiFetchData,
  apiAddItemToShip,
  apiAddItemToDock
} from '@modules/port/actions'

import './Port.css'

class Port extends Component {
  componentDidMount() {
    this.props.fetchData()
  }

  onDropHandler(cargoItem, target) {
    if (!target) {
      return
    }

    if (target.type && target.type === 'ship') {
      this.dispatch(apiAddItemToShip(cargoItem, target))
    } else if (target.type && target.type === 'dock') {
      this.dispatch(apiAddItemToDock(cargoItem, target))
    }
  }

  render() {
    let ships = null

    if (this.props.port.ships.length) {
      ships = this.props.port.ships.map((ship) => (
        <Ship
          ship={ ship }
          cargoItems={ ship.cargoItems}
          id={ ship.id }
          key={ ship.id }
          onDrop={ this.onDropHandler }
        />
      ))
    } else {
      ships = (
        <div className="message-container">
          <h3>No ships at the port currently...</h3>
        </div>
      )
    }

    if (this.props.port.failedToLoad) {
      ships = (
        <div className="message-container">
          <h3>Failed to load data from server :(</h3>
        </div>
      )
    }

    return (
      <div className="port-container">
        <AlertDialog />
        <EditItemDialog />
        <Notification />
        <Grid container spacing={24}>
          <Grid item xs={5}>
            <div className="dock__column">
              <Dock
                dock={ this.props.port.dock }
                cargoItems= { this.props.port.dock.cargoItems }
                onDrop={ this.onDropHandler }
              />
            </div>
          </Grid>
          <Grid item xs={7}>
            <div className="ship__column">
              {
                this.props.port.loadingData ?
                  <Loader loadingText={"Ships and cargo items are loading"} /> :
                  ships
              }
            </div>
          </Grid>
        </Grid>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    port: state.port,
    dialog: state.dialog,
    notifications: state.notifications
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchData: () => dispatch(apiFetchData())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Port, axios)
