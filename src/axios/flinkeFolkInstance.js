import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://demos.dev.flinkefolk.lt/Home/'
})

export default instance