export const SHOW_ALERT_DIALOG = 'SHOW_ALERT_DIALOG'
export const SHOW_EDIT_DIALOG = 'SHOW_EDIT_DIALOG'

export function showAlertDialog(show) {
  return { type: SHOW_ALERT_DIALOG, payload: { show }}
}

export function showEditDialog(show, editingItem = {}) {
  return { type: SHOW_EDIT_DIALOG, payload: { show, editingItem }}
}