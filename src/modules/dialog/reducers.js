import { SHOW_ALERT_DIALOG, SHOW_EDIT_DIALOG } from './actions'
const initialState = {
  isShowingAlertDialog: false,
  isShowingEditDialog: false,
  editingItem: {}
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case (SHOW_ALERT_DIALOG): {
      const { show } = action.payload

      return {
        ...state,
        isShowingAlertDialog: show
      }
    }
    case (SHOW_EDIT_DIALOG): {
      const { show, editingItem } = action.payload

      return {
        ...state,
        isShowingEditDialog: show,
        editingItem
      }
    }
    default:
      return {
        ...state
      }
  }
}

export default reducer