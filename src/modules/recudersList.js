import dialog from './dialog/reducers'
import notifications from './notifications/reducers'
import port from './port/reducers'

export default {
  dialog,
  notifications,
  port
}