import { SHOW_NOTIFICATION, HIDE_NOTIFICATION } from './actions'

const initialState = {
  isOpen: false,
  message: '',
  queue: [],
  variant: 'success'
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case (SHOW_NOTIFICATION): {
      const { message, color } = action.payload

      return {
        ...state,
        isOpen: true,

        message,
        color
      }
    }
    case (HIDE_NOTIFICATION): {
      return {
        ...state,
        isOpen: false
      }
    }
    default:
      return {
        ...state
      }
  }
}

export default reducer