export default {
  success: '#4caf50',
  error: '#f44336',
  warning: '#ffa726'
}