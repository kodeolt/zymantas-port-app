export const SHOW_NOTIFICATION = 'SHOW_NOTIFICATION'
export const HIDE_NOTIFICATION = 'HIDE_NOTIFICATION'

export function notificationInit(message, color = '#4caf50', delay = 1300) {
  return dispatch => {
    dispatch(showNotification(message, color))

    setTimeout(() => dispatch(hideNotification()), delay)
  }
}

export function showNotification(message, color = '') {
  return { type: SHOW_NOTIFICATION, payload: { message, color } }
}

export function hideNotification() {
  return { type: HIDE_NOTIFICATION }
}