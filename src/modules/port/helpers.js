/**
 * Get total cargo weight
 * @param {array} cargoItems
 * @return double number
 */
export const getTotalCargoWeight = (cargoItems) => {
  let weight = cargoItems.reduce((weight, item) => {
    return Number(item.weight) + Number(weight)
  }, 0)

  return Number(weight.toFixed(2))
}

/**
 * Get total cargo volume
 * @param {array} cargoItems
 * @return double number
 */
export const getTotalCargoVolume = (cargoItems) => {
  let volume = cargoItems.reduce((volume, item) => {
    return Number(item.volume) + Number(volume)
  }, 0)

  return Number(volume.toFixed(2))
}

/**
 * Calculate percentage of current load
 * @param {double} currentValue
 * @param {double} maxValue
 * @return number
 */
export const loadPercentage = (currentValue, maxValue) => {
  return ((currentValue * 100) / maxValue).toFixed(1)
}