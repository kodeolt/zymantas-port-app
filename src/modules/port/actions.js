import axios from '../../axios/flinkeFolkInstance'
import uuid from 'uuid/v4'

import { notificationInit } from '@modules/notifications/actions'
import notificationColors from '@modules/notifications/colorMap'

import {
  USER_GUID,
  GET_PORT_DATA_URL,
  CREATE_CARGO_ITEM_URL,
  EDIT_CARGO_ITEM_URL,
  ADD_ITEM_TO_DOCK_URL,
  ADD_ITEM_TO_SHIP_URL
} from '../../variables'


export const LOADED_PORT_DATA = 'LOADED_PORT_DATA'
export const FAILED_TO_LOAD_DATA = 'FAILED_TO_LOAD_DATA'

export const ADD_CARGO_ITEM = 'ADD_CARGO_ITEM'
export const ATTACH_CARGO_ITEM_ID = 'ATTACH_CARGO_ITEM_ID'
export const REMOVE_CARGO_ITEM_BY_UUID = 'REMOVE_CARGO_ITEM_BY_UUID'

export const EDIT_CARGO_ITEM = 'EDIT_CARGO_ITEM'

export const ADD_ITEM_TO_DOCK = 'ADD_ITEM_TO_DOCK'
export const ADD_ITEM_TO_SHIP = 'ADD_ITEM_TO_SHIP'

export const REVERSE_ACTION = 'REVERSE_ACTION'
export const RESTORE_CARGO_ITEM = 'RESTORE_CARGO_ITEM'
export const CLEAR_ACTION_HISTORY = 'CLEAR_ACTION_HISTORY'

/**
 * Set loaded data to state
 * @param {object} data
 */
export function loadedPortData(data) {
  return { type: LOADED_PORT_DATA, payload: { data } }
}

/**
 * Failed to load data
 */
export function failedToLoadData() {
  return { type: FAILED_TO_LOAD_DATA }
}

/**
 * Add cargo to state
 * @param {object} cargo
 */
export function addCargoItem(cargo) {
  return { type: ADD_CARGO_ITEM, payload: { cargo } }
}

/**
 * Attach ID to cargo item
 * @param {int} cargoId
 * @param {string} tempUuid
 */
export function attachCargoItemId(cargoId, tempUuid) {
  return { type: ATTACH_CARGO_ITEM_ID, payload: { cargoId, tempUuid } }
}

/**
 * Remove cargo item by its temp uuid
 * @param {string} tempUuid
 */
export function removeCargoItemByUuid(tempUuid) {
  return { type: REMOVE_CARGO_ITEM_BY_UUID, payload: { tempUuid } }
}

/**
 * Update cargo item at state
 * @param {object} cargo
 * @param {string} actionUuid
 */
export function editCargoItem(cargo, actionUuid) {
  return { type: EDIT_CARGO_ITEM, payload: { cargo, actionUuid } }
}

/**
 * Add item to the dock state
 * @param {object} cargo
 * @param {object} target
 * @param {string} actionUuid
 */
export function addItemToDock(cargo, target, actionUuid) {
  return { type: ADD_ITEM_TO_DOCK, payload: { cargo, target, actionUuid } }
}

/**
 * Add item to the ship state
 * @param {object} cargo
 * @param {object} target
 * @param {string} actionUuid
 */
export function addItemToShip(cargo, target, actionUuid) {
  return { type: ADD_ITEM_TO_SHIP, payload: { cargo, target, actionUuid } }
}

/**
 * If adding to the dock/ship request failed,
 * restore previous state
 * @param {object} cargo
 * @param {object} target
 * @param {string} actionUuid
 */
export function reverseAction(cargo, target, actionUuid) {
  return { type: REVERSE_ACTION , payload: { cargo, target, actionUuid }}
}

/**
 * Clear history list item
 * @param {string} actionUuid
 */
export function clearActionHistory(actionUuid) {
  return { type: CLEAR_ACTION_HISTORY, payload: { actionUuid }}
}

/**
 * Restore cargo item
 * @param {int} cargoId
 * @param {string} actionUuid
 */
export function restoreCargoItem(cargoId, actionUuid) {
  return { type: RESTORE_CARGO_ITEM, payload: { cargoId, actionUuid } }
}

/**
 * ---------------------------------------------------
 * API REQUESTS
 * ---------------------------------------------------
 */

/**
 * Fetch port data from server
 */
export function apiFetchData() {
  return dispatch => {
    axios.post(GET_PORT_DATA_URL, {
      userGuid: USER_GUID
    })
      .then((response) => {
        dispatch(notificationInit(`Loaded data successfully`, notificationColors.success))

        const { data } = response
        dispatch(loadedPortData(data))
      })
      .catch(() => {
        dispatch(failedToLoadData())
        dispatch(notificationInit(`Failed to load data from server :(`, notificationColors.error))
      })
  }
}

/**
 * Send API request to store new cargo item
 * If request is success, attach got ID to item
 * If request failed, remove added cargo item
 * @param {object} cargo
 */
export function apiAddCargoItem(cargo) {
  return dispatch => {
    const tempUuid = uuid()
    cargo.tempUuid = tempUuid
    dispatch(addCargoItem(cargo))

    axios.post(CREATE_CARGO_ITEM_URL, {
      userGuid: USER_GUID,
      weight: cargo.weight,
      volume: cargo.volume
    })
      .then(response => {
        dispatch(attachCargoItemId(response.data, tempUuid))
        dispatch(notificationInit('Cargo created', notificationColors.success))
      })
      .catch(() => {
        dispatch(removeCargoItemByUuid(tempUuid))
        dispatch(notificationInit('Failed to create cargo', notificationColors.error))
      })
  }
}

/**
 * Send API request to edit cargo item
 * @param {object} cargo
 */
export function apiEditCargoItem(cargo) {
  return dispatch => {
    const actionUuid = uuid()
    dispatch(editCargoItem(cargo, actionUuid))

    axios.post(EDIT_CARGO_ITEM_URL, {
      userGuid: USER_GUID,
      cargoId: cargo.id,
      weight: cargo.weight,
      volume: cargo.volume
    })
      .then(() => dispatch(notificationInit('Cargo updated', notificationColors.warning)))
      .catch(() => {
        dispatch(restoreCargoItem(cargo.id, actionUuid))
        dispatch(notificationInit('Failed to updated cargo', notificationColors.error))
      })
      .finally(() => {
        dispatch(clearActionHistory(actionUuid))
      })
  }
}

/**
 * Send API request to add item to the dock
 * @param {object} cargo
 * @param {object} target
 */
export function apiAddItemToDock(cargo, target) {
  return dispatch => {
    const actionUuid = uuid()
    dispatch(addItemToDock(cargo, target, actionUuid))

    axios.post(ADD_ITEM_TO_DOCK_URL, {
      userGuid: USER_GUID,
      cargoId: cargo.id
    })
      .then(() => dispatch(notificationInit('Cargo moved to dock', notificationColors.success)))
      .catch(() => {
        dispatch(reverseAction(cargo, target, actionUuid))
        dispatch(notificationInit('Failed to move cargo', notificationColors.error))
      })
      .finally(() => {
        dispatch(clearActionHistory(actionUuid))
      })
  }
}

/**
 * Send API request to add item to the ship
 * @param {object} cargo
 * @param {object} target
 */
export function apiAddItemToShip(cargo, target) {
  return dispatch => {
    const actionUuid = uuid()
    dispatch(addItemToShip(cargo, target, actionUuid))

    axios.post(ADD_ITEM_TO_SHIP_URL, {
      userGuid: USER_GUID,
      cargoId: cargo.id,
      shipId: target.targetShip.id
    })
      .then(() => dispatch(notificationInit(`Cargo moved to ship "${target.targetShip.name}"`, notificationColors.success)))
      .catch(() => {
        dispatch(reverseAction(cargo, target, actionUuid))
        dispatch(notificationInit('Failed to move cargo', notificationColors.error))
      })
      .finally(() => {
        dispatch(clearActionHistory(actionUuid))
      })
  }
}