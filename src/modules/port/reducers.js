import _ from 'lodash'

import {
  LOADED_PORT_DATA,
  ADD_CARGO_ITEM,
  ATTACH_CARGO_ITEM_ID,
  EDIT_CARGO_ITEM,
  ADD_ITEM_TO_DOCK,
  ADD_ITEM_TO_SHIP,
  REVERSE_ACTION,
  RESTORE_CARGO_ITEM,
  CLEAR_ACTION_HISTORY,
  REMOVE_CARGO_ITEM_BY_UUID,
  FAILED_TO_LOAD_DATA
} from './actions'

/**
 * Initial Port State
 * Port has DOCK and SHIPS
 * Dock holds available cargo items
 * While Ships store cargo items from dock (MAX 5 Ships at the same time in port)
 */
const initialState = {
  loadingData: false,
  failedToLoad: false,
  dock: {
    cargoItems: []

  },
  ships: [],
  addingNewCargoItem: false,
  history: []
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case (LOADED_PORT_DATA): {
      /**
       * Should get port data
       * dock cargo items,
       * and ships with cargo items in port
       */
      const { dock, ships } = action.payload.data

      if (ships.length > 5) {
        ships.slice(0, 5)
      }

      if (dock.cargoItems.length > 200) {
        dock.cargoItems.slice(0, 200)
      }

      return {
        ...state,
        dock,
        ships
      }
    }
    case (FAILED_TO_LOAD_DATA): {
      return {
        ...state,
        failedToLoad: true
      }
    }
    case (ADD_CARGO_ITEM): {
      const { cargo } = action.payload

      const dock = _.cloneDeep(state.dock)
      dock.cargoItems.push(cargo)

      return {
        ...state,
        dock
      }
    }
    case (ATTACH_CARGO_ITEM_ID): {
      const { cargoId, tempUuid } = action.payload

      const dock = _.cloneDeep(state.dock)
      const dockCargoItemIndex = dock.cargoItems.findIndex(item => item.tempUuid === tempUuid)

      if (dockCargoItemIndex !== -1) {
        dock.cargoItems[dockCargoItemIndex].id = cargoId
      }

      return {
        ...state,
        dock
      }
    }
    case (REMOVE_CARGO_ITEM_BY_UUID): {
      const { tempUuid } = action.payload

      const dock = {...state.dock}
      dock.cargoItems = dock.cargoItems.filter(item => item.tempUuid !== tempUuid)

      return {
        ...state,
        dock
      }
    }
    case (EDIT_CARGO_ITEM): {
      const { cargo } = action.payload

      const history = [...state.history]
      history.push({
        uuid: action.payload.actionUuid,
        state: _.cloneDeep(state)
      })

      const dock = {...state.dock}
      const ships = _.cloneDeep(state.ships)

      const dockCargoItems = [...dock.cargoItems]

      const foundDockCargoIndex = dockCargoItems.findIndex(item => item.id === cargo.id)

      if (foundDockCargoIndex !== -1) {
        const copyCargo = {...dockCargoItems[foundDockCargoIndex]}
        copyCargo.weight = cargo.weight
        copyCargo.volume = cargo.volume

        dockCargoItems[foundDockCargoIndex] = copyCargo

        dock.cargoItems = dockCargoItems
      } else {
        let shipId = null

        for (let ship of ships) {
          for (let item of ship.cargoItems) {
            if (item.id === cargo.id) {
              shipId = ship.id
            }
          }
        }

        const shipIndex = ships.findIndex(ship => ship.id === shipId)
        const cargoIndex = ships[shipIndex].cargoItems.findIndex(item => item.id === cargo.id)
        ships[shipIndex].cargoItems[cargoIndex] = cargo
      }


      return {
        ...state,
        history,
        dock,
        ships
      }
    }
    case (ADD_ITEM_TO_DOCK): {
      const history = [...state.history]
      history.push({
        uuid: action.payload.actionUuid,
        state: _.cloneDeep(state)
      })

      const { cargo } = action.payload

      const dock = {...state.dock}
      const ships = [...state.ships]
      const cargoItems = [...dock.cargoItems]

      if (cargoItems.length >= 200) {
        return {
          ...state
        }
      }

      for (let ship of ships) {
        ship.cargoItems = ship.cargoItems.filter((item) => item.id !== cargo.id)
      }

      cargoItems.push(cargo)
      dock.cargoItems = cargoItems

      return {
        ...state,
        history,
        ships,
        dock
      }
    }
    case (ADD_ITEM_TO_SHIP): {
      const history = [...state.history]
      history.push({
        uuid: action.payload.actionUuid,
        state: _.cloneDeep(state)
      })

      const { cargo } = action.payload
      const { targetShip } = action.payload.target

      const ships = [...state.ships]
      const dock = {...state.dock}

      // Remove item from source ship
      for (let ship of ships) {
        ship.cargoItems = ship.cargoItems.filter(item => item.id !== cargo.id)
      }
      dock.cargoItems = dock.cargoItems.filter(item => item.id !== cargo.id)

      // Add cargo to ship
      const targetShipIndex = ships.findIndex(ship => ship.id === targetShip.id)

      const cargoItems = [...ships[targetShipIndex].cargoItems]
      cargoItems.push(cargo)
      ships[targetShipIndex].cargoItems = cargoItems

      return {
        ...state,
        history,
        ships,
        dock
      }
    }
    case (REVERSE_ACTION): {
      // Put it to source target
      // Remove from destination target
      const { cargo, target, actionUuid } = action.payload

      const history = state.history.find(item => {
        return item.uuid === actionUuid
      })

      const historyDock = _.cloneDeep(history.state.dock)
      const historyShips = _.cloneDeep(history.state.ships)

      const dock = _.cloneDeep(state.dock)
      const ships = _.cloneDeep(state.ships)

      let sourceWasFound = false

      // Find where cargo was
      for (let item of historyDock.cargoItems) {
        if (item.id === cargo.id) {
          const dockCargoItems = [...dock.cargoItems]
          dockCargoItems.push(cargo)
          dock.cargoItems = dockCargoItems
        }
      }

      if (!sourceWasFound) {
        let sourceShipId = null

        for (let historyShip of historyShips) {
          for (let item of historyShip.cargoItems) {
            if (item.id === cargo.id) {
              sourceShipId = historyShip.id
            }
          }
        }

        if (sourceShipId) {
          const sourceShipIndex = ships.findIndex(ship => ship.id === sourceShipId)
          const shipCargoItems = [...ships[sourceShipIndex].cargoItems]

          shipCargoItems.push(cargo)
          ships[sourceShipIndex].cargoItems = shipCargoItems
        }
      }

      if (target.targetShip) {
        const targetShipIndex = ships.findIndex(ship => ship.id === target.targetShip.id)
        const shipCargoItems = [...ships[targetShipIndex].cargoItems].filter(item => item.id !== cargo.id)
        ships[targetShipIndex].cargoItems = shipCargoItems
      }

      if (target.targetDock) {
        dock.cargoItems = dock.cargoItems.filter(item => item.id !== cargo.id)
      }

      return {
        ...state,
        dock,
        ships
      }
    }
    case (CLEAR_ACTION_HISTORY): {
      const { actionUuid } = action.payload
      const history = state.history.filter(action => action.uuid !== actionUuid)

      return {
        ...state,
        history
      }
    }
    case (RESTORE_CARGO_ITEM): {
      const { cargoId, actionUuid } = action.payload

      const history = state.history.find(item => {
        return item.uuid === actionUuid
      })

      const historyDock = history.state.dock
      const dock = _.cloneDeep(state.dock)

      const historyCargoItems = [...historyDock.cargoItems]
      const currentCargoItems = [...dock.cargoItems]

      const restoredCargoItem = historyCargoItems.find(item => item.id === cargoId)
      const currentCargoItemIndex = currentCargoItems.findIndex(item => item.id === cargoId)

      currentCargoItems[currentCargoItemIndex] = restoredCargoItem

      dock.cargoItems = currentCargoItems

      return {
        ...state,
        dock
      }
    }
    default:
      return state
  }
}



export default reducer