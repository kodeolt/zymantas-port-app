import React from 'react'
import HTML5Backend from 'react-dnd-html5-backend'
import { DragDropContext } from 'react-dnd'
import Port from '@containers/port/Port'

function App() {
  return (
    <div className="App">
      <Port />
    </div>
  )
}

export default DragDropContext(HTML5Backend)(App)
