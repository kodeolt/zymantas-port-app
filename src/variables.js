export const USER_GUID = '04213BF1-9C5D-4439-9FF8-125B064FCDAD'

export const GET_PORT_DATA_URL = '/GetInitData'
export const CREATE_CARGO_ITEM_URL = '/CreateCargo'
export const EDIT_CARGO_ITEM_URL = '/EditCargo'
export const ADD_ITEM_TO_SHIP_URL = '/AddToShip'
export const ADD_ITEM_TO_DOCK_URL = '/AddToDock'