import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import './Loader.css'

export default (props) => (
  <div className="loader-container">
    <div className="loader">
      <CircularProgress />
      <p>{ props.loadingText || 'Content is loading' }</p>
    </div>
  </div>
)