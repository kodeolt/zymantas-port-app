import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'

import './Notification.css'

const notifications = (props) => {
  const backgroundColor = props.notifications.color

  return (
    <Snackbar
      className="custom-snackbar"
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      open={ props.notifications.isOpen }>
      <SnackbarContent
        className="custom-snackbar__content"
        style={{ backgroundColor }}
        message={ props.notifications.message.toUpperCase() } />
    </Snackbar>
  )
}

notifications.propTypes = {
  notifications: PropTypes.object.isRequired
}

const mapStateToProps = state => {
  return {
    notifications: state.notifications
  }
}

export default connect(mapStateToProps, null)(notifications)

