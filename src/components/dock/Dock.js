import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { DropTarget } from 'react-dnd'

import { getTotalCargoVolume, getTotalCargoWeight } from '@modules/port/helpers'

import Item from '../item/Item'
import Header from './partials/Header/Header'

import './Dock.css'

const dock = (props) => {
  let cargoItems = <p className="message">Dock has no items</p>

  if (props.cargoItems.length) {
    cargoItems = (
      <ul className="cargo-items">
        { props.cargoItems.map((cargo) => (
          <li key={ cargo.id || Date.now() }>
            <Item cargoItem={ cargo }/>
          </li>
        ))}
      </ul>
    )
  }

  const { connectDropTarget, hovered } = props
  const backgroundColor = hovered ? '#e6e7ee': null

  return connectDropTarget(
    <div className="dock-container">
      <div className="dock">
        <Header
          totalItems={ props.cargoItems.length }
          totalVolume={ getTotalCargoVolume(props.cargoItems) }
          totalWeight={ getTotalCargoWeight(props.cargoItems) }
        />
        <div className="items-container" style={{ backgroundColor }}>
          { cargoItems }
        </div>
      </div>
    </div>
  )
}

dock.propTypes = {
  dock: PropTypes.object.isRequired,
  cargoItems: PropTypes.array.isRequired
}

const collect = (connect, monitor) => {
  return {
    connectDropTarget: connect.dropTarget(),
    hovered: monitor.isOver(),
    item: monitor.getItem()
  }
}

const spec = {
  drop(props, monitor) {
    const cargoItem = monitor.getItem()

    for (let item of props.dock.cargoItems) {
      if (item.id === cargoItem.id) {
        return
      }
    }

    const target = {
      type: 'dock',
      targetDock: props.dock
    }

    props.onDrop(cargoItem, target)
  }
}

export default connect(null, null)(DropTarget('cargoItem', spec, collect)(dock))