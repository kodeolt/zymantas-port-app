import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

import './AddCargoItem.css'

class AddCargoItem extends Component {
  state = {
    cargo: {
      weight: 0,
      volume: 0
    }
  }

  onVolumeChangeHandler(e) {
    const volume = Number(e.target.value).toFixed(2)
    const cargo = {...this.state.cargo}

    cargo.volume = volume

    this.setState({ cargo })
  }

  onWeightChangeHandler(e) {
    const weight = Number(e.target.value).toFixed(2)
    const cargo = {...this.state.cargo}

    cargo.weight = weight

    this.setState({ cargo })
  }

  render() {
    return (
      <div className="dock__form-container">
        <p>Add New Item</p>
        <form className="dock__form">
          <div className="form__group">
            <TextField
              label="Weight"
              defaultValue={ this.state.cargo.weight }
              onChange={ (e) => this.onWeightChangeHandler(e) }
            />
            <TextField
              label="Volume"
              defaultValue={ this.state.cargo.volume }
              onChange={ (e) => this.onVolumeChangeHandler(e) }
            />
          </div>
          <div className="form__group pull-right">
            <Button
              color="secondary"
              onClick={ () => this.props.close() }
            >Close</Button>
            <Button
              variant="contained"
              color="primary"
              onClick={ () => {
                this.props.createItem(this.state.cargo)
                this.props.close()
              } }
            >Create</Button>
          </div>
        </form>
      </div>
    )
  }
}

export default AddCargoItem