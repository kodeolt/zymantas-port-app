import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Button from '@material-ui/core/Button';
import AddCargoItem from '../AddItem/AddCargoItem'
import { apiAddCargoItem } from '@modules/port/actions'

import './Header.css'

class Header extends Component {
  state = {
    isCreatingCargoItem: false
  }

  onAddHandler() {
    let isCreatingCargoItem = !this.state.isCreatingCargoItem
    this.setState({ isCreatingCargoItem })
  }

  onCreateItemHandler(cargo) {
    this.props.dispatch(apiAddCargoItem(cargo))
  }

  render() {
    let buttonContainer = (
      <Button variant="contained" color="primary" onClick={ () => this.onAddHandler() }>
        Add Item
      </Button>
    )

    return (
      <div className="header">
        <div className="header__top">
          <code>Dock</code>
          { this.state.isCreatingCargoItem
            ? <AddCargoItem
                createItem={ (cargo) => this.onCreateItemHandler(cargo) }
                close={ () => this.onAddHandler() }
              />
            : buttonContainer
          }
        </div>
        <div className="header__bottom">
          <div className="column">
            <span>Total Items:</span> <br/>
            <strong>{ this.props.totalItems }</strong> / 200
          </div>
          <div className="column">
            <span>Total Volume:</span> <br/>
            <strong>{ this.props.totalVolume }</strong>
          </div>
          <div className="column">
            <span>Total Weight:</span> <br/>
            <strong>{ this.props.totalWeight }</strong>
          </div>
        </div>
      </div>
    )
  }
}

Header.propTypes = {
  totalItems: PropTypes.number.isRequired,
  totalVolume: PropTypes.number.isRequired,
  totalWeight: PropTypes.number.isRequired
}

export default connect(null, null)(Header)