import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { DragSource } from 'react-dnd'

import { showEditDialog } from '@modules/dialog/actions'

import DragIndicatorIcon from '@material-ui/icons/DragIndicator'
import EditIcon from '@material-ui/icons/Edit'
import './Item.css'


const itemSource = {
  beginDrag(props) {
    return props.cargoItem
  },

  endDrag(props, monitor, component) {
    if (!monitor.didDrop()) {
      return
    }
  }
}

const collect = (connect, monitor) => {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  }
}

const item = (props) => {
  const { isDragging, connectDragSource } = props
  const opacity = isDragging ? 0 : 1
  const visibility = isDragging ? 'hidden' : 'visible'

  return connectDragSource(
    <div className="cargo-item" style={{ opacity, visibility }}>
      <div>
        <DragIndicatorIcon />
        <strong><code>{ props.cargoItem.id }</code></strong>
        <span> Weight: <strong>{ props.cargoItem.weight }</strong>. Volume: <strong>{ props.cargoItem.volume }</strong></span>
        <EditIcon className="edit-icon" onClick={() => props.showEditDialog(true, props.cargoItem)} />
      </div>
    </div>
  )
}

item.propTypes = {
  cargoItem: PropTypes.object.isRequired
}

const mapDispatchToProps = dispatch => {
  return {
    showEditDialog: (show, editingItem) => dispatch(showEditDialog(show, editingItem))
  }
}

export default connect(null, mapDispatchToProps)(DragSource('cargoItem', itemSource, collect)(item))