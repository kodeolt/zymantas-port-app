import React from 'react'
import { connect } from 'react-redux'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from '@material-ui/core/TextField'

import { showEditDialog } from '@modules/dialog/actions'
import { apiEditCargoItem } from '@/modules/port/actions'

const dialog = (props) => {
  const updatingCargoItem = {
    id: props.dialog.editingItem.id,
    volume: props.dialog.editingItem.volume,
    weight: props.dialog.editingItem.weight
  }

  const onVolumeChangeHandler = (e) => {
    const volume = Number(e.target.value).toFixed(2)
    updatingCargoItem.volume = volume
  }

  const onWeightChangeHandler = (e) => {
    const weight = Number(e.target.value).toFixed(2)
    updatingCargoItem.weight = weight
  }

  const onSaveHandler = () => {
    props.editCargoItem(updatingCargoItem)
    props.showEditDialog(false)
  }

  return (
    <Dialog
      open={ props.dialog.isShowingEditDialog }
      onClose={ () => props.showEditDialog(false) }
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Editing Cargo Item - <code>{props.dialog.editingItem.id}</code></DialogTitle>
      <DialogContent>
        <form>
          <TextField
            label="Weight"
            defaultValue={ props.dialog.editingItem.weight }
            onChange={ (e) => onWeightChangeHandler(e) }
          />
          <TextField
            label="Volume"
            defaultValue={ props.dialog.editingItem.volume }
            onChange={ (e) => onVolumeChangeHandler(e) }
          />
        </form>
      </DialogContent>
      <DialogActions>
        <Button 
          color="secondary"
          autoFocus
          onClick={ () => props.showEditDialog(false) }>
          Close
        </Button>
        <Button 
          variant="contained"
          color="primary"
          autoFocus
          onClick={ () => onSaveHandler() } >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  )
}

const mapStateToProps = state => {
  return {
    dialog: state.dialog
  }
}

const mapDispatchToProps = dispatch => {
  return {
    showEditDialog: (show) => dispatch(showEditDialog(show)),
    editCargoItem: (cargo) => dispatch(apiEditCargoItem(cargo))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(dialog)