import React from 'react'
import { connect } from 'react-redux'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

import { showAlertDialog } from '@modules/dialog/actions'

const dialog = (props) => {

  return (
    <Dialog
      open={ props.dialog.isShowingAlertDialog }
      onClose={ () => props.showAlertDialog(false) }
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Warning!</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Ship's capacity is exceeded! Cargo item will not be added.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={ () => props.showAlertDialog(false) } color="primary" autoFocus>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
}

const mapStateToProps = state => {
  return {
    dialog: state.dialog
  }
}

const mapDispatchToProps = dispatch => {
  return {
    showAlertDialog: (show) => dispatch(showAlertDialog(show))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(dialog)