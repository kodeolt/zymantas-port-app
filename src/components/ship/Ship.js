import React from 'react'
import { connect } from 'react-redux'
import { DropTarget } from 'react-dnd'
import PropTypes from 'prop-types'

import { getTotalCargoVolume, getTotalCargoWeight, loadPercentage } from '@modules/port/helpers'
import { showAlertDialog } from '@modules/dialog/actions'

import DirectionsBoatIcon from '@material-ui/icons/DirectionsBoat'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Item from '../item/Item'
import './Ship.css'

// Component
const ship = (props) => {
  let cargoItems = ''

  if (props.cargoItems.length) {
    cargoItems = (
      <ul className="cargo-items">
        { props.cargoItems.map((cargo) => (
          <li key={ cargo.id }>
            <Item cargoItem={ cargo }/>
          </li>
        ))}
      </ul>
    )
  }

  const { connectDropTarget, hovered } = props
  const backgroundColor = hovered ? '#e6e7ee': '#fff'

  const currentCargoWeight = getTotalCargoWeight(props.cargoItems)
  const currentCargoVolume = getTotalCargoVolume(props.cargoItems)
  const currentWeightPercentage = loadPercentage(currentCargoWeight, props.ship.maxWeight)
  const currentVolumePercentage = loadPercentage(currentCargoVolume, props.ship.maxVolume)

  return connectDropTarget(
    <div className="ship">
      <div className="ship-container" style={{ backgroundColor }}>
        <div className="ship__header">
          <DirectionsBoatIcon className="icon"/>
          <h3 className="name">{ props.ship.name } <code>({ props.ship.id})</code></h3>
        </div>
        <div className="ship__details">
          <div className="column">
            <span>Weight:</span> <br/>
            <strong>{ currentCargoWeight } / { props.ship.maxWeight } <br /><code>({currentWeightPercentage}% loaded)</code></strong>
          </div>
          <div className="column">
            <span>Volume:</span> <br/>
            <strong>{ currentCargoVolume } / { props.ship.maxVolume } <br /><code>({currentVolumePercentage}% loaded)</code></strong>
          </div>
        </div>
          <ExpansionPanel className="cargo-items__expansion">
            <ExpansionPanelSummary className="expansion__summary">
              <span>Cargo Items <code>(Total - { props.cargoItems.length })</code></span>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className="expansion__items">
              { cargoItems }
            </ExpansionPanelDetails>
          </ExpansionPanel>
      </div>
    </div>
  )
}

ship.propTypes = {
  ship: PropTypes.object.isRequired
}

const collect = (connect, monitor) => {
  return {
    connectDropTarget: connect.dropTarget(),
    hovered: monitor.isOver(),
    item: monitor.getItem()
  }
}

const spec = {
  drop(props, monitor, component) {
    const cargoItem = monitor.getItem()

    const targetShip = props.ship

    // If dropping on same target, do nothing
    for (let item of targetShip.cargoItems) {
      if (item.id === cargoItem.id) {
        return
      }
    }

    // If ships capacity is exceeded, show alert
    if (
      getTotalCargoVolume(targetShip.cargoItems) + cargoItem.volume > targetShip.maxVolume ||
      getTotalCargoWeight(targetShip.cargoItems) + cargoItem.weight > targetShip.maxWeight
    ) {
      props.dispatch(showAlertDialog(true))
      return
    }

    const target = {
      type: 'ship',
      targetShip: targetShip
    }
    props.onDrop(cargoItem, target)
  }
}

export default connect(null, null)(DropTarget('cargoItem', spec, collect)(ship))