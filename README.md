# Port App

Author: <strong>Žymantas Venclauskas</strong><br>
Total time: 13 hours

## Built App

Open `build/Index.html` file to run app.

## Development Scripts

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm build`

Build production app. All files will appear in `build` folder.
